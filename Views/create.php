
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Add Work History</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<form method="POST" action="store.php">
    <fieldset>
        <legend><h3>Add New Work History</h3></legend>
        <table border="0" cellpadding="5" cellspacing="5">
        	<tr>
        		<th><label>Worker Name: </label></th>
        		<td><input type="text" name="name" autofocus>
       			 <?php 
	           	 if(isset($_SESSION['name']) && !empty($_SESSION['name'])){
		                echo $_SESSION['name'];
		                unset($_SESSION['name']);
		            }
            		?>
            	</td>
        	</tr>
        	<tr>
        		<th><label>Working Date:</label> </th>
        		<td><input type="date" name="workingDate"></td>
        	</tr>
        	<tr>
        		<th><label>Collecting Amount: </label></th>
        		<td>
        			People: <input type="number" name="people">
       				Friend Zero: <input type="number" name="friendZero">
       				Friend: <input type="number" name="friend">
        		</td>
        	</tr>
        	<tr>
        		<td><input type="reset" value="Clear All"></td>
        		<td><input type="submit" value="Submit Data"></td>
        	</tr>
        </table>       
    </fieldset>
</form>
</body>
</html>